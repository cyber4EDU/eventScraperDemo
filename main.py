import requests
from bs4 import BeautifulSoup
from attrs import define
from typing import Optional


@define
class Event:
    title: str
    date: str
    time: str
    location: str
    image: Optional[str]
    lab: Optional[str]
    url: Optional[str]


# https://jugendhackt.org/wp-sitemap.xml
base_urls = ["https://jugendhackt.org", "https://jugendhackt.org/kalender/"]

for base_url in base_urls:
    response = requests.get(base_url)

    if response.status_code == 200:
        html_doc = response.text
        # soup = BeautifulSoup(html_doc, 'xml')
        soup = BeautifulSoup(html_doc, 'html.parser')
        result = soup.find_all(name='div', class_='event-teaser-list-item', recursive=True)
        for r in result:
            eventTitle = r.find(name='h3', class_='mb-0').string

            if eventTitle is not None:
                time_string = r.time.string.strip().split('|')
                eventDate = time_string[0].split(' ')[1].strip()
                eventTime = time_string[1].strip()
                eventLocation = r.find(name='div', class_='c-uppercase-title').string
                eventImageTag = r.find(name='img')
                if eventImageTag is not None:
                    eventImage = eventImageTag['src']
                else:
                    eventImage = None
                eventLab = r.find(name='div', class_='c-uppercase-title').string.split(' ')[1].strip()
                eventUrlTag = r.find(name='a')
                if eventUrlTag is not None:
                    eventUrl = eventUrlTag['href']
                else:
                    eventUrl = None

                event = Event(
                    eventTitle,
                    eventDate,
                    eventTime,
                    eventLocation,
                    eventImage,
                    eventLab,
                    eventUrl,
                )

                print(event.title, ' am: ', event.date, ' um: ', event.time, ' Ort: ', event.location, " Image URL: ",
                      event.image, ' Lab: ', event.lab, ' Lab URL: ', event.url)
    else:
        print(f"could not load data: {response.text}")
